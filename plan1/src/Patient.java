import java.util.Scanner;

public class Patient {
    Double temperature;
    double topBloodPressure;
    double bottomBloodPressure;
    double weight;
    double height;
    String[] symptoms = new String[2];

    Scanner input = new Scanner(System.in);

    public Patient(){

    }
    public Patient(Double temperature, double topBloodPressure, double bottomBloodPressure, double weight, double height, String[] symptoms) {
        this.temperature = temperature;
        this.topBloodPressure = topBloodPressure;
        this.bottomBloodPressure = bottomBloodPressure;
        this.weight = weight;
        this.height = height;
        this.symptoms = symptoms;
    }

    // and all getters and setters here
    public double getTemperature() {
        return temperature;
    }
    public void setTemperature() {
        System.out.println("Please look at the thermometer and type in the current body temperature");
        this.temperature = input.nextDouble();
    }
    public double getTopBloodPressure() {
        return topBloodPressure;
    }

    public void setTopBloodPressure() {
        System.out.println("Please type your current top blood pressure");
        this.topBloodPressure = input.nextDouble();
    }

    public double getBottomBloodPressure() {
        return bottomBloodPressure;
    }

    public void setBottomBloodPressure() {
        System.out.println("Please type your current bottom blood pressure");
        this.bottomBloodPressure = input.nextDouble();
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight() {
        System.out.println("Please type in your weight, do not be shy, we are all professionals");
        this.weight = input.nextDouble();
    }

    public double getHeight() {
        return height;
    }

    public void setHeight() {
        System.out.println("Please type your height(in meters)");
        this.height = input.nextDouble();
    }

    public String[] getSymptoms() {
        return symptoms;
    }

    public void setSymptoms() {
        System.out.print("Please pick and type in 2 of listed symptoms (no capital letters) :\n - headache\n - tiredness\n - dizziness\n - toothache\n - chills\n - sadness\n - sleepy\n");
            for (int i = 0; i < 2; i++) {
                symptoms[i] = input.next();
        }

    }


}
